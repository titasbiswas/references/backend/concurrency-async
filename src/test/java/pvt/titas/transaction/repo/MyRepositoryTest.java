package pvt.titas.transaction.repo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pvt.titas.transaction.entities.MyEntity;

import static org.junit.Assert.assertNotNull;

@SpringBootTest
public class MyRepositoryTest {

    @Autowired
    private MyRepository myRepository;

    @Test
    public void testVersion() {
        MyEntity myEntity = MyEntity.builder()
                .id(1L)
                .data("data 1")
                .desc("desc 1")
                .build();

        myRepository.save(myEntity);
        myRepository.findAll().forEach(System.out::println);
        myRepository.save(myEntity);
        myRepository.findAll().forEach(System.out::println);
        myEntity.setDesc("desc 2");
        myRepository.save(myEntity);

        myRepository.findAll().forEach(System.out::println);
        assertNotNull(myRepository);
    }

}