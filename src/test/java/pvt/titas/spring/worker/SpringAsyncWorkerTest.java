package pvt.titas.spring.worker;

import org.assertj.core.api.AssertionsForInterfaceTypes;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringAsyncWorkerTest {

    @Autowired
    private SpringAsyncWorker worker;

    @Test
    public void testCreateMyObj() {

        List<CompletableFuture<MyObject>> completableFutureList = new ArrayList<>();

        IntStream.range(0, 25).forEach(i ->{
            try {
                completableFutureList.add(worker.createMyObj(i));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        //Wait till all tasks are completed
        completableFutureList.stream().map(CompletableFuture::join).collect(Collectors.toList());


            completableFutureList.stream().forEach(cf ->{
                try {
                    System.out.println("Number: "+cf.get().getNumber()+" Thread Name: "+cf.get().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            });

        //Just to make the test run
        Assert.assertNotNull(completableFutureList);

    }

}