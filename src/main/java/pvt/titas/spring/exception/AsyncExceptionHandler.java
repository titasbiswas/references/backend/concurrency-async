package pvt.titas.spring.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import java.lang.reflect.Method;

public class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncExceptionHandler.class);

    public AsyncExceptionHandler() {
    }

    @Override
    public void handleUncaughtException(Throwable throwable, Method method, Object... obj) {
        LOGGER.info("Exception message - " + throwable.getMessage());
        LOGGER.info("Method name - " + method.getName());
        for (Object param : obj) {
            LOGGER.info("Parameter value - " + param);
        }
    }
}
