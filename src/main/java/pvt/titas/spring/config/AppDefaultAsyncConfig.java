package pvt.titas.spring.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import pvt.titas.spring.exception.AsyncExceptionHandler;

import java.util.concurrent.Executor;

@Configuration
@EnableAsync
public class AppDefaultAsyncConfig implements AsyncConfigurer {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppDefaultAsyncConfig.class);


    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(20);
        //executor.setQueueCapacity(10);
        executor.setKeepAliveSeconds(60*60);
        executor.setThreadNamePrefix("SpringAsync-");
        executor.initialize();
        LOGGER.info("Initialized ThreadPoolTaskExecutor: " + executor);

        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new AsyncExceptionHandler();
    }
}
