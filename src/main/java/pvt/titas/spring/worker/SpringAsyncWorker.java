package pvt.titas.spring.worker;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Component
public class SpringAsyncWorker {

    @Async
    public CompletableFuture<MyObject> createMyObj(int id) throws InterruptedException {
        Thread.sleep(1000);
        MyObject myObject = new MyObject(id);
        return CompletableFuture.completedFuture(myObject);
    }
}
