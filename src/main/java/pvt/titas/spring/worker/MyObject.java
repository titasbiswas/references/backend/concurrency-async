package pvt.titas.spring.worker;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MyObject {

    private int number;
    private String name;

    public MyObject(int number){
        this.number=number;
        this.name=Thread.currentThread().getName();
    }
}
