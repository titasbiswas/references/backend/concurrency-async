package pvt.titas.transaction.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pvt.titas.transaction.entities.MyEntity;

@Repository
public interface MyRepository extends JpaRepository<MyEntity, Long> {
}
