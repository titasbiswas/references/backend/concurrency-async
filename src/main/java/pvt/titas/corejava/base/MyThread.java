package pvt.titas.corejava.base;

//class extending Thread
public class MyThread extends Thread {
    public static void main(String[] args) {
        Thread t1 = new MyThread();
        t1.start(); // return immediately, won't wait till run() is completed
        System.out.println("t1 started");

        //inner class Thread
        Thread t2 = new Thread() {
            public void run() {
                System.out.println(Thread.currentThread().getName() + " : run()");
            }
        };
        t2.start();
        System.out.println("t2 started");

        Thread t3 = new Thread(new MyRunnable());
        t3.start();
        System.out.println("t3 started");

        //inner class of runnable
        Thread t4 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " : run()");
            }
        });
        t4.start();
        System.out.println("t4 started");

        //Runnable with lambda
        Runnable myLambdaRunnable = () -> System.out.println(Thread.currentThread().getName() + " : run()");
        Thread t5 = new Thread(myLambdaRunnable);
        t5.start();
        System.out.println("t5 started");

        //stoppable runnable
        StoppableRunnable r = new StoppableRunnable();
        Thread t6 = new Thread(r);
        t6.start();
        try {
            Thread.sleep(10L * 1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Stopping t6");
        r.doStop();

    }

    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // get the current thread by Thread.currentThread()
        System.out.println(Thread.currentThread().getName() + " : run()");

    }
}

//implementing runnable
class MyRunnable implements Runnable {

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " : run()");
    }
}

//stop() is deprecated as it may cause instability of programme
class StoppableRunnable implements Runnable {

    boolean running = true;

    public synchronized void doStop() {
        running = false;
    }

    private synchronized boolean keepRunning(){
        return this.running;
    }

    @Override
    public void run() {
        while (keepRunning()) {
            try {
                Thread.sleep(3L * 1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "running");
        }

    }
}

