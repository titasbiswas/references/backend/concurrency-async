package pvt.titas.corejava.executorservices;

import java.util.concurrent.*;

public class VariousExcuterServices {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Callable<Integer> callable = () -> {
            int data = 10;

            Thread.sleep(1000);
            return data;
        };

        Runnable runnable = ()->{
            try {
                int data = 0;
                System.out.println("Run "+data);
                data++;
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        // Creates a single thread ExecutorService
        ExecutorService singleExecutorService = Executors.newSingleThreadExecutor();
        // Creates a single thread ScheduledExecutorService
        ScheduledExecutorService singleScheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        // Creates an ExecutorService that use a pool of 10 threads
        ExecutorService fixedExecutorService = Executors.newFixedThreadPool(10);
        // Creates an ExecutorService that use a pool that creates threads on demand
        // And that kill them after 60 seconds if they are not used
        ExecutorService onDemandExecutorService = Executors.newCachedThreadPool();
        // Creates a ScheduledExecutorService that use a pool of 5 threads
        ScheduledExecutorService fixedScheduledExecutorService = Executors.newScheduledThreadPool(5);
        //Creates a cached thread pool
        ExecutorService cachedExecutorService = Executors.newCachedThreadPool();



        System.out.println(singleExecutorService.submit(callable).get());

        System.out.println(cachedExecutorService.submit(callable).get());


        // Will start command1 after 50 seconds
        singleScheduledExecutorService.schedule(runnable, 50L, TimeUnit.SECONDS);

        // Will start command 2 after 20 seconds, 25 seconds, 30 seconds ...
        fixedScheduledExecutorService.scheduleAtFixedRate(runnable, 20L, 5L, TimeUnit.SECONDS);


        // Will start command 3 after 10 seconds and if command3 takes 2 seconds to be
        // executed also after 17, 24, 31, 38 seconds...
        fixedScheduledExecutorService.scheduleWithFixedDelay(runnable, 10L, 5L, TimeUnit.SECONDS);
    }
}
