package pvt.titas.corejava.completable_future;

import lombok.Data;

import java.util.concurrent.*;
import java.util.function.Supplier;

public class Demo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //--------- base case c1
        CompletableFuture<Void> c1 = CompletableFuture.runAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Run c1 on thread: " + Thread.currentThread().getName());
        });
        c1.join();

        //-------- return result, with supplier, c2
        CompletableFuture<String> c2 = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return "returned result from c2";
            }
        });
        System.out.println(c2.get());

        //-------- return result, with supplier, with executor service, c3
        ExecutorService executor = Executors.newFixedThreadPool(2);
        CompletableFuture c3 = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "returned result from c3 with thread: " + Thread.currentThread().getName();
        }, executor);
        System.out.println(c3.get());
        executor.shutdown();


        //*************** Callback
        //c4
        // --------thenApply(),  It takes a Function<T,R>,
        // that accepts an argument of type T and produces a result of type R
        //We can also write a sequence of transformations on the CompletableFuture by
        // attaching a series of thenApply() callback methods.
        // The result of one thenApply() method is passed to the next in the series.
        String userName = "John";
        CompletableFuture<String> c4 = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Hello ";
        })
                .thenApply(str -> str + userName)
                .thenApply(str -> str + ", how are you?");
        System.out.println(c4.get());

        //c5
        // ----------thenAccept() - returns CompletableFuture<Void>, takes a consumer
        CompletableFuture.supplyAsync(()->{
            return 1.0;
        }).thenAccept(i->{
            System.out.println("c5 === adding up with 2: "+(i+2));
                });

        //c6
        // ----------thenRun() - returns CompletableFuture<Void>, does not have access to result
        CompletableFuture.supplyAsync(()->{
            return 1.0;
        }).thenAccept(i->{
            System.out.println("c6 === adding up with 2: "+(i+2));
        })
        .thenRun(()->
                System.out.println(" c6 was running on thread: "+Thread.currentThread().getName()));

        //***************** Asynchronous Callback
        //c7 - async version of the above methods which runs on different threads than the thread where
        // supplyAsync() has been executed (or in the main thread if the supplyAsync() task completes immediately)
        executor = Executors.newCachedThreadPool();
        CompletableFuture.supplyAsync(()->{
            System.out.print("c7 ===returning from thread: ");
            return Thread.currentThread().getName();
        }).thenApplyAsync(str->{
            System.out.print(str+ "then returning from thread: ");
            return Thread.currentThread().getName();
        }, executor).thenAcceptAsync(str->{
            System.out.print(str+ "and running on  thread: "+Thread.currentThread().getName());
        }).thenRunAsync(()->
                System.out.print("; Finally  running on  thread: "+Thread.currentThread().getName()+"\n")
        );
        executor.shutdown();

        //*****************************Combining CompletableFutures together
        //c8
        //-------- thenCompose(), flatten the futures like flatMap
        //Difference with thenApply() => use thenApply() if you have a synchronous mapping function like
        //CompletableFuture.supplyAsync(() -> 1).thenApply(x -> x+1);
        // use thenCompose() if have an asynchronous mapping function (i.e. one that returns a CompletableFuture)
        //CompletableFuture.supplyAsync(() -> 1).thenCompose(x -> CompletableFuture.supplyAsync(() -> x+1));
        CompletableFuture<Product> productDetails = getProduct("1234")
                .thenCompose(p->getProductDiscount(p));
        System.out.println("c8==="+productDetails.get());

        //c9
        //thenCombine() - use to combine completable futures which are independent of each other
        String productId="a23fre345";
        CompletableFuture<Product> price = CompletableFuture.supplyAsync(()->{
            //mimics a service call
            Product product = new Product();
            product.setId(productId);
            product.setPrice(100.00);
            product.setDiscount(5.0);
            return product;
        });
        CompletableFuture<Product> desc =  CompletableFuture.supplyAsync(()->{
            //mimics a service call
            Product product = new Product();
            product.setId(productId);
            product.setDesc("A nice cool product");
            return product;
        });

        CompletableFuture<Product> product = price.thenCombine(desc, (p, d)->{
            Product newProduct = new Product();
            newProduct.setId(p.getId());
            newProduct.setPrice(p.getPrice());
            newProduct.setDiscount(p.getDiscount());
            newProduct.setDesc(d.getDesc());
            return newProduct;
        });
        System.out.println("c9==="+product.get());
    }
    static CompletableFuture<Product> getProduct(String productId){
        return CompletableFuture.supplyAsync(()->{
            //get product from a service
            Product product = new Product();
            product.setId(productId);
            return product;
        });
    }

    static CompletableFuture<Product> getProductDiscount(Product product){
        return CompletableFuture.supplyAsync(()->{
            //get discount from a service
            Product product1 = new Product();
            product1.setId(product.getId());
            product1.setDiscount(5.0);
            return product1;
        });
    }

    @Data
    static
    class Product{
        String id, desc;
        Double price, discount;
    }


}
