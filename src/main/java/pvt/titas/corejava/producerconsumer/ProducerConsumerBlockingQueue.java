package pvt.titas.corejava.producerconsumer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class ProducerConsumerBlockingQueue {

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Integer> q = new LinkedBlockingDeque<>(2);

        Thread producer = new Thread(()->{
            try {
                int data = 0;
                while(true){
                    q.add(data);
                    System.out.println("Produced "+data);
                    data++;
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread consumer = new Thread(()->{
            try{
                while (true){
                    int data = q.take();
                    System.out.println("Consumed "+ data);
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        producer.start();
        consumer.start();


        producer.join();
        consumer.join();
    }
}
