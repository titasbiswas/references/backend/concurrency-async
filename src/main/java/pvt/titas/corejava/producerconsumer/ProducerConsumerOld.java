package pvt.titas.corejava.producerconsumer;

import java.nio.Buffer;
import java.util.LinkedList;
import java.util.Queue;

public class ProducerConsumerOld {

    public static void main(String[] args) throws InterruptedException {
        Buffer q = new Buffer(2);


        Thread producer = new Thread(() -> {
            try {
                int data = 0;
                while (true) {
                    q.add(data);
                    System.out.println("Produced " + data);
                    data++;
                    Thread.sleep(1000);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        Thread consumer = new Thread(() -> {
            try {
                while (true) {
                    int data = q.poll();
                    System.out.println("Consumed " + data);

                    Thread.sleep(1000);

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        producer.start();
        consumer.start();

        producer.join();
        consumer.join();

    }

    static class Buffer {
        private Queue<Integer> q;
        private int size;

        public Buffer(int size) {
            this.q = new LinkedList<>();
            this.size = size;
        }

        public void add(int data) throws InterruptedException {
            synchronized (this) {
                while (q.size() >= size) {
                    wait();
                }
                q.add(data);
                notify();
            }
        }

        public int poll() throws InterruptedException {
            synchronized (this) {
                while (q.size() <= 0) {
                    wait();
                }
                int data = q.poll();
                notify();
                return data;
            }
        }
    }
}
