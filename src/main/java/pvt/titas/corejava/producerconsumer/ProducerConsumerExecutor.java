package pvt.titas.corejava.producerconsumer;

import java.util.Queue;
import java.util.concurrent.*;

public class ProducerConsumerExecutor {

    public static void main(String[] args) {
        BlockingQueue<Integer> q = new LinkedBlockingDeque<>(2);

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        Runnable producer = ()->{
            try{
               int data = 0;
               while (true){
                   q.put(data);
                   System.out.println("Produces "+data);
                   data++;
                   Thread.sleep(1000);
               }
            } catch (Exception e) {
                e.printStackTrace();
            }

        };

        Runnable consumer = ()->{
            try{
                while (true){
                    int data = q.take();
                    System.out.println("Consumes "+data);
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        executorService.execute(producer);
        executorService.execute(consumer);

        executorService.shutdown();


    }
}
