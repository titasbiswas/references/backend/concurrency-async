All primitive local variables are thread safe

`public void someMethod(){
   long threadSafeInt = 0;
   threadSafeInt++;
}`
 
 Local references are thread safe, but objects are created in shared heap.
 If an object created locally never escapes the method it was created in, it is thread safe.

`public void someMethod(){
    LocalObject localObject = new LocalObject();
    localObject.callMethod();
    method2(localObject);
}
public void method2(LocalObject localObject){
    localObject.setValue("value");
}`

Member variables may lead to race condition if threads access the same instance

`public class NotThreadSafe{
    StringBuilder builder = new StringBuilder();
    public add(String text){
        this.builder.append(text);
    }
}`


`NotThreadSafe sharedInstance = new NotThreadSafe();    
        new Thread(new MyRunnable(sharedInstance)).start();
        new Thread(new MyRunnable(sharedInstance)).start();       
        public class MyRunnable implements Runnable{
          NotThreadSafe instance = null;       
          public MyRunnable(NotThreadSafe instance){
            this.instance = instance;
          }       
          public void run(){
            this.instance.add("some text");
          }
        }`